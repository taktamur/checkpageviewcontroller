//
//  PAMViewController.h
//  CheckPageView
//
//  Created by Takafumi Tamura on 2013/09/02.
//  Copyright (c) 2013年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAMViewController : UIViewController<UIPageViewControllerDataSource>
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
