//
//  PAMViewController.m
//  CheckPageView
//
//  Created by Takafumi Tamura on 2013/09/02.
//  Copyright (c) 2013年 田村 孝文. All rights reserved.
//

#import "PAMViewController.h"

@interface PAMViewController ()
@property(readonly)UIPageViewController *pageViewController;
@end

@implementation PAMViewController{
    NSMutableArray *_controllers;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // controllerを生成しておく。
    _controllers = [[NSMutableArray alloc]init];
    [_controllers addObject: [self.class viewControllerWithColor:[UIColor redColor]]];
    [_controllers addObject: [self.class viewControllerWithColor:[UIColor blueColor]]];
    
    // pageViewControllerの設定
    // Storyboardで transitionStyleをscrollに設定している
    self.pageViewController.dataSource=self;
    [self.pageViewController setViewControllers:@[_controllers[0]]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self switchPage:@(1)];
}

-(void)switchPage:(NSNumber *)number
{
    // ページ送り/戻ししている間は、ユーザの操作を無行にする
    self.pageViewController.view.userInteractionEnabled=NO;

    // ページ送り/戻し
    NSUInteger index = number.intValue;
    self.textLabel.text = [NSString stringWithFormat:@"%d",index];
    UIPageViewControllerNavigationDirection direction =
        index==0 ? UIPageViewControllerNavigationDirectionReverse:UIPageViewControllerNavigationDirectionForward;
    __block UIViewController *controller = _controllers[index];
    [self.pageViewController setViewControllers:@[controller]
                                      direction:direction
                                       animated:YES
                                     completion:^(BOOL finished) {
                                         // ページ送り/戻しが終わったら、ユーザの操作を有効にする
                                         self.pageViewController.view.userInteractionEnabled=YES;
//                                         if(finished)
//                                         {
//                                             dispatch_async(dispatch_get_main_queue(), ^{
//                                                 [self.pageViewController
//                                                  setViewControllers:@[controller]
//                                                  direction:UIPageViewControllerNavigationDirectionForward
//                                                  animated:NO
//                                                  completion:^(BOOL finished) {
//
//                                                  }];
//                                             });
//                                         }
                                     }];
    // 次のループ開始
    NSUInteger nextIndex = index==0 ? 1 : 0;
    [self performSelector:@selector(switchPage:)
               withObject:@(nextIndex)
               afterDelay:1.0];
}


-(UIPageViewController *)pageViewController
{
    for (UIViewController *c in self.childViewControllers) {
        if( [c isKindOfClass:UIPageViewController.class] ){
            return (UIPageViewController *)c;
        }
    }
    return nil;
}

#pragma mark - UIPageViewControllerDataSource
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerAfterViewController:(UIViewController *)viewController
{
    if( [viewController isEqual:_controllers[0]] ){
        return _controllers[1];
    }else{
        return nil;
    }
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
     viewControllerBeforeViewController:(UIViewController *)viewController
{
    if( [viewController isEqual:_controllers[1]] ){
        return _controllers[0];
    }else{
        return nil;
    }
}

#pragma mark - util
+(UIViewController *)viewControllerWithColor:(UIColor *)color
{
    UIViewController *c = [[UIViewController alloc]init];
    c.view.backgroundColor=color;
    return c;
}

@end
